
''' Generate different cluster type and cluster it with your choice of clustering algorithms.
Use the ``bokeh serve`` command to run the example by executing:
    bokeh serve main.py
at your command prompt. Then navigate to the URL
    http://localhost:5006/main
in your browser.
All right reserved by Pooyan Shirvani Ghomi.
'''


import numpy as np

from bokeh.io import curdoc
import bokeh.palettes as palettes
from bokeh.layouts import layout, row, column
from bokeh.models import ColumnDataSource,HoverTool, Button
from bokeh.models.widgets import Slider, TextInput, Select
from bokeh.plotting import figure
from bokeh.events import ButtonClick
from generate_cluster import make_a_cluster
from cluster_methods import apply_clustering_method
from create_options_box import create_cluster_type_options, create_clustering_method_options, parse_paseed_values
from copy import copy


color_map = palettes.inferno

# Set up widgets
cluster_type_selection = Select(title="Cluster Type", value=None, options=["Blobs","Blobs Varied","Circles","Gaussian Quantiles","Moon","S Curve","Swiss Roll","No Structure","Ansio"])
tools_to_show = "crosshair,pan,reset,save,wheel_zoom,hover"

clustering_method_selection = Select(title="Clustering Method", value=None, options=["KMeans","MiniBatchKMeans","MeanShift","Birch","DBSCAN","AffinityPropagation","Agglomerative","SpectralClustering"])
source = ColumnDataSource(dict(samples_d_1=[],samples_d_2=[],samples=[],true_label=[], r_label=[]))
button_generate_cluster = Button(label='Generate Cluster')
button_cluster_data = Button(label='Cluster')
cluster_type_options_section = []

# Set up callbacks
def choose_cluster_type(attar, old, new):
    cluster_type_choosen = copy(str(cluster_type_selection.value).replace(' ','_').replace('\n','').lower())
    cluster_type_options = create_cluster_type_options(cluster_type_choosen)
    names = cluster_type_options.keys()
    cluster_type_options_section= [cluster_type_options[x] for x in names].copy()

    if len(layout_1.children)>1:
        layout_1.children[1]=column(cluster_type_options_section)
    else:
        layout_1.children.append(column(cluster_type_options_section))

    if len(layout_1.children) <3:
        layout_1.children.append(row(button_generate_cluster))
    else:
        layout_1.children[2]=(row(button_generate_cluster))

def choose_clustering_method(attar, old, new):
    clustering_method_choosen = copy(str(clustering_method_selection.value))
    clustering_method_options = create_clustering_method_options(clustering_method_choosen)
    names = clustering_method_options.keys()
    clustering_method_options_section= [clustering_method_options[x] for x in names].copy()

    if len(layout_2.children)>1:
        layout_2.children[1]=column(clustering_method_options_section)
    else:
        layout_2.children.append(column(clustering_method_options_section))

    if len(layout_2.children) <3:
        layout_2.children.append(row(button_cluster_data))
    else:
        layout_2.children[2]=(row(button_cluster_data))


def generate_cluster(event):
    cluster_type_chosen = copy(str(cluster_type_selection.value).replace(' ','_').replace('\n','').lower())
    cluster_type_options = create_cluster_type_options(cluster_type_chosen)

    names = list(cluster_type_options.keys())
    name_values = {}
    for i in range(len(names)):
        name_values[names[i]] = layout_1.children[1].children[i].children[0].value

    values = parse_paseed_values(name_values)
    values.update({'cluster_type':cluster_type_chosen})
    X, t_label = make_a_cluster(**values)
    source.update()
    source.data = dict(samples_d_1=X[:,0],samples_d_2=X[:,1],samples=X,true_label=t_label)

    if len(layout_2.children)<2:
        layout_2.children.append(layout(clustering_method_selection))


def run_clustering_method(event):
    clustering_method_chosen = copy(str(clustering_method_selection.value))
    clustering_method_chosen_options= create_clustering_method_options(clustering_method_chosen)

    names = list(clustering_method_chosen_options.keys())
    values = {}
    for i in range(len(names)):
        values[names[i]] = layout_2.children[1].children[i].children[0].value
    values.update({'method_type':clustering_method_chosen, 'samples':source.data['samples']})
    r_label = apply_clustering_method(**values)
    source.data.update(dict(r_label=r_label))
    unique_labels = np.unique(source.data['r_label'])
    color_mapping = color_map(unique_labels.shape[0])
    colors = [color_mapping[x] for x in source.data['r_label']]
    source.data.update(dict(color_mapping=colors))
    alpha = 0.6
    TOOLTIPS=[("Label", "@r_label"),]
    title = "{0} using method {1}".format(cluster_type_selection.value, clustering_method_chosen)
    plot = figure(plot_height=800, plot_width=800, title=title,tools="crosshair,pan,reset,save,wheel_zoom")
    layout_3.children=[plot]
    plot.circle(x='samples_d_1', y='samples_d_2', source=source, color='color_mapping', fill_alpha=0.5)
    plot.add_tools(HoverTool(tooltips=TOOLTIPS, show_arrow=False, point_policy='follow_mouse'))

cluster_type_selection.on_change('value', choose_cluster_type)
clustering_method_selection.on_change('value', choose_clustering_method)
button_generate_cluster.on_event(ButtonClick, generate_cluster)
button_cluster_data.on_event(ButtonClick, run_clustering_method)



# Set up layouts and add to document
layout_1 = layout(cluster_type_selection)
layout_2 = layout()
layout_3 = layout()


sizing_mode = 'fixed'
curdoc().add_root(row(layout_1, layout_2, layout_3))
curdoc().title = "Cluster experiment"