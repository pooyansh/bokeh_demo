from sklearn import datasets
import numpy as np
from sklearn.preprocessing import StandardScaler


def make_ansio(n_samples, random_state, transformation, **kwargs):
    X, y = datasets.make_blobs(n_samples=n_samples,random_state=random_state, **kwargs)
    X_ansio = np.dot(X, transformation)
    return (X_ansio, y)


def make_blobs_varied(n_samples, random_state, cluster_std, centers, **kwargs):
    varied = datasets.make_blobs(n_samples=n_samples, random_state=random_state,
                                 cluster_std=cluster_std,centers=centers,
                                 **kwargs)
    return varied


def make_no_structure(n_samples, random_state=None):
    np.random.seed(random_state)
    return np.random.rand(n_samples,2), np.random.randint(2,size=(n_samples,1))


def make_swiss_roll(n_samples,random_state,noise=0):
    np.random.seed(random_state)
    which_2 = np.random.randint(0,3,2,dtype=int)
    X = datasets.make_swiss_roll(n_samples,noise,random_state)
    return X[0][:,which_2], X[1]


def make_a_cluster(sample_size, cluster_type, normalize=False, random_state=None, **kwargs):
    __name__ = r"make_a_cluster"
    __doc__ = r"a helper function to create toy data for testing different clustering" \
              r" methods using sklearn.datasets."
    """
    :param sample_size: The total number of point in clusters; the larger the sample size increase run time;
    :param cluster_type: Type of cluster your of generate. We use sklearn.datasets to generate a toy example
    :param random_state: If int, random_state is the seed used by the random number generator;
    :param kwargs: extra kwargs that user wants to pass to cluster generator function.
    :return: X array of shape[ n_sample, 2], y array of shape[n_samples] labels (0 or 1).
    """
    """cluster_functions_available = dict(blobs=datasets.make_blobs,
                                       blobs_varied=make_blobs_varied,
                                       circles=datasets.make_circles,
                                       gaussian_quantiles=datasets.make_gaussian_quantiles,
                                       moon=datasets.make_moons,
                                       s_curve=datasets.make_s_curve,
                                       swiss_roll=datasets.make_swiss_roll,
                                       no_structure=make_no_structure,
                                       ansio=make_ansio)
    
    """
    cluster_functions_available = {'blobs':datasets.make_blobs,
                                       'blobs_varied':make_blobs_varied,
                                       'circles':datasets.make_circles,
                                       'gaussian_quantiles':datasets.make_gaussian_quantiles,
                                       'moon':datasets.make_moons,
                                       's_curve':datasets.make_s_curve,
                                       'swiss_roll':datasets.make_swiss_roll,
                                       'no_structure':make_no_structure,
                                       'ansio':make_ansio}
    try:
        cluster_fun = cluster_functions_available[cluster_type]
    except KeyError:
        print("We are sorry! We could not generate cluster type you are requested. The available type are: "
              "{0}".format(", ".join(cluster_functions_available.keys())))
        exit(-1)
    default_base = {'n_samples': sample_size,
                    'random_state': random_state}

    default_values = dict(blobs={'centers': 3},
                          blobs_varied ={'centers': 3,'cluster_std': [0.3*np.pi, 2, 2*np.pi]},
                          circles={'noise': 0.05},
                          gaussian_quantiles={'n_features': 2},
                          moon={'noise': 0.1},
                          s_curve={'noise': 0.1},
                          swiss_roll={'noise': 0.1},
                          no_structure={},
                          ansio={'centers': 2, 'transformation': [[0.6, -0.6], [-0.4, 0.8]] }
                          )

    default_base.update(default_values[cluster_type])
    default_base.update(kwargs)
    X, y = cluster_fun(**default_base)
    if normalize is True:
        X = StandardScaler().fit_transform(X)

    return X, y


if __name__ == '__main__':
    X,y = make_a_cluster(sample_size=1500, cluster_type='gaussian_quantiles')

