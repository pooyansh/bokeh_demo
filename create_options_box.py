__init__ = ['create_cluster_type_options','create_clustering_method_options']
from bokeh.layouts import layout
from bokeh.layouts import row, widgetbox
from bokeh.models import Button
from bokeh.models.widgets import Slider, TextInput, Select
import numpy as np
import os


def create_clustering_method_options(clustering_method):
    section_structure={
        "KMeans":{
            "n_clusters": Slider(title='Number of Clusters', value=8, start=1, end=100, step=1),
            "random_state": Slider(title='Random State', value=42, start=1, end=1000, step=1),
            "init": Select(title="Cluster Type", value="k-means++",options=["k-means++","random"]),
            "n_init": Slider(title='Number algorithm run', value=10, start=1, end=50, step=1),
            "max_iter": Slider(title='Maximum Iter.', value=300, start=100, end=1000, step=20),
            "tol": Slider(title='Tolerance', value=1e-4, start=1e-6, end=1e-2, step=1e-6),
            #"precompute_distances": Select(title="precompute_distances", value="auto",options=['True','False', "auto"]),
            "n_jobs": Slider(title='Number of CPUs', value=1, start=1, end=os.cpu_count(), step=1),
            "algorithm": Select(title="Algorithm", value="auto",options=["auto","full","elkan"]),
            },
        "MiniBatchKMeans":{
            "n_clusters": Slider(title='Number of Clusters', value=8, start=1, end=100, step=1),
            "init": Select(title="Cluster Type", value="k-means++",options=["k-means++","random"]),
            "max_iter": Slider(title='Maximum Iter.', value=300, start=100, end=1000, step=20),
            "batch_size": Slider(title='Maximum Iter.', value=100, start=10, end=10000, step=10),
            "random_state": Slider(title='Random State', value=42, start=1, end=1000, step=1),
            "tol": Slider(title='Tolerance', value=1e-4, start=1e-6, end=1e-2, step=1e-6),
            "max_no_improvement": Slider(title='Max No. Improvement', value=10, start=1, end=100, step=1),
            "n_int": Slider(title='Number algorithm run', value=10, start=1, end=50, step=1),
            "reassignment_ratio": Slider(title='Reassignment Ratio', value=1e-2, start=1e-2, end=9e-1, step=1e-2),
            },
        "MeanShift":{
            "bandwidth": Slider(title='Bandwidth', value=0.1, start=0, end=1, step=1e-2),
            "min_bin_freq": Slider(title='min_bin_freq', value=1, start=1, end=100, step=1),
            "n_jobs": Slider(title='Number of CPUs', value=1, start=1, end=os.cpu_count(), step=1),
        },
        "Birch":{
            "threshold":Slider(title='Threshold', value=0.1, start=0, end=1, step=1e-2),
            "branching_factor":Slider(title='Branching Factor', value=50, start=10, end=1000, step=10),
            "n_clusters":Slider(title='Number of Clusters', value=8, start=1, end=100, step=1),
        },
        "DBSCAN":{
                "eps": Slider(title='Epsilon', value=0.1, start=0.01, end=0.5, step=1e-2),
                "min_samples": Slider(title='min_samples', value=50, start=10, end=1000, step=10),
                "algorithm": Select(title="Algorithm", value="auto",options=["auto", "ball_tree", "kd_tree", "brute"]),
                "leaf_size": Slider(title='Leaf Size', value=30, start=10, end=300, step=10),
                "p": Slider(title="Power of Minkowski Metric", value=2, start=0.1, end=10, step=0.1),
                "n_jobs": Slider(title='Number of CPUs', value=1, start=1, end=os.cpu_count(), step=1)
            },
        "AffinityPropagation":{
                "damping": Slider(title='Damping', value=0.5, start=0.5, end=1, step=1e-2),
                "max_iter": Slider(title='Maximum Iter.', value=200, start=100, end=1000, step=20),
                "convergence_iter": Slider(title='Convergence Iter.', value=15, start=5, end=200, step=5),
                "preference": Slider(title='Preference.', value=10, start=-200, end=200, step=5)
            },
        "Agglomerative":{
            "n_clusters": Slider(title='Number of Clusters', value=8, start=1, end=100, step=1),
            "affinity": Select(title="Affinity", value="euclidean",options=["euclidean", "l1", "l2", "manhattan", "cosine"]),
            "linkage": Select(title="Linkage", value="ward",options=["ward", "complete", "average"])
            },
        "SpectralClustering":{
            "n_clusters": Slider(title='Number of Clusters', value=8, start=1, end=100, step=1),
            "eigen_solver": Select(title="Eigenvalue Solver", value="amg",options=[ "arpack", "amg", "lobpcg"]),
            "eigen_tol": Slider(title='eigen_tol', value=0, start=0, end=1e-2, step=1e-6),
            "random_state": Slider(title='Random State', value=42, start=1, end=1000, step=1),
            "n_int": Slider(title='Number algorithm run', value=10, start=1, end=50, step=1),
            "gamma": Slider(title='gamma', value=1.0, start=0, end=10, step=0.01),
            "affinity": Select(title="Affinity", value="rbf",options=["rbf", "nearest_neighbors"]),
            "n_neighbors": Slider(title='Number of Neighbors', value=10, start=1, end=100, step=1),
            "assign_labels": Select(title="Assign_labels", value="kmeans",options=[ "kmeans", "discretize"]),
            "degree": Slider(title='Coefficient', value=1, start=1, end=10, step=1),
            "coef0": Slider(title='Coefficient', value=1, start=1, end=10, step=1),
            "n_jobs": Slider(title='Number of CPUs', value=1, start=1, end=os.cpu_count(), step=1)
        }
    }
    try:
        method_section = section_structure[clustering_method]
    except KeyError:
        print("We are sorry! We could not generate cluster type you are requested.")
        exit(-1)

    return method_section


def create_cluster_type_options(cluster_type):
    section_structure= {
        'blobs':
        {
            'sample_size':Slider(title='Sample Size', value=1500, start=100, end=100000, step=100),
            'random_state':Slider(title='Random State', value=42, start=1, end=1000, step=1),
            'centers':Slider(title='Centers', value=3, start=1, end=100, step=1),
        },
        'blobs_varied':
        {
            'sample_size':Slider(title='Sample Size', value=1500, start=100, end=100000, step=100),
            'random_state':Slider(title='Random State', value=42, start=1, end=1000, step=1),
            'centers':Slider(title='Centers', value=3, start=1, end=100, step=1),
            'cluster_std':TextInput(title='Standard deviation of centers', value='1,2,3'),
            'center_box_min':Slider(title='Centers Box Min', value=-10, start=-100, end=100, step=0.1),
            'center_box_max':Slider(title='Centers Box Max', value=10, start=-100, end=100, step=0.1),
        },
        'circles':
        {
            'sample_size':Slider(title='Sample Size', value=1500, start=100, end=100000, step=100),
            'random_state':Slider(title='Random State', value=42, start=1, end=1000, step=1),
            'noise':Slider(title='Noise', value=1, start=0.01, end=10, step=0.01),
            'factor':Slider(title='Dist. between inner and outer', value=0.8, start=0.01, end=1, step=0.01),
        },
        'gaussian_quantiles':
        {
            'sample_size':Slider(title='Sample Size', value=1500, start=100, end=100000, step=100),
            'random_state':Slider(title='Random State', value=42, start=1, end=1000, step=1),
            'mean':TextInput(title='Mean',value='0,0'),
            'cov':Slider(title='Covariance', value=1, start=0.00, end=10, step=0.1),
            'n_classes':Slider(title='Number of Classes',value=3, start=1, end=100, step=1)
        },
        'moon':
        {
            'sample_size':Slider(title='Sample Size', value=1500, start=1, end=100000, step=100),
            'random_state':Slider(title='Random State', value=42, start=1, end=1000, step=1),
            'noise':Slider(title='Noise', value=1, start=0.01, end=100, step=0.1),
        },
        's_curve':
        {
            'sample_size':Slider(title='Sample Size', value=1500, start=1, end=100000, step=100),
            'random_state':Slider(title='Random State', value=42, start=1, end=1000, step=1),
            'noise':Slider(title='Noise', value=1, start=0.01, end=100, step=0.1),
        },
        'swiss_roll':
        {
            'sample_size':Slider(title='Sample Size', value=1500, start=1, end=100000, step=100),
            'random_state':Slider(title='Random State', value=42, start=1, end=1000, step=1),
           'noise':Slider(title='Noise', value=1, start=0.01, end=100, step=0.1),

        },
        'no_structure':
        {
            'sample_size':Slider(title='Sample Size', value=1500, start=1, end=100000, step=100),
            'random_state':Slider(title='Random State', value=42, start=1, end=1000, step=1),
        },
        'ansio':
        {
            'sample_size':Slider(title='Sample Size', value=1500, start=1, end=100000, step=100),
            'random_state':Slider(title='Random State', value=42, start=1, end=1000, step=1),
            'centers':Slider(title='Centers', value=3, start=1, end=100, step=1),
            'transformation':TextInput(title='Transformation', value='0.6,-0.6,-0.4,0.8')
        }

    }
    try:
        cluster_section = section_structure[cluster_type]
    except KeyError:
        print("We are sorry! We could not generate cluster type you are requested.")
        exit(-1)

    return cluster_section


def make_a_matrix(str_1):
    list_1 = str_1.split(',')
    list_1 = [np.float64(x) for x in list_1]
    return np.array(list_1).reshape(2,2)


def make_a_vector(str_1):
    list_1 = str_1.split(',')
    list_1 = [np.float64(x) for x in list_1]
    return np.array(list_1)

def parse_paseed_values(dict_of_values):
    fun_dict = {
        'transformation': make_a_matrix,
        'cluster_std': make_a_vector,
        'mean': make_a_vector
    }
    for key in dict_of_values.keys():
        if key in fun_dict.keys():
            dict_of_values[key] = fun_dict[key](dict_of_values[key])
    if 'center_box_min' in dict_of_values:
        if dict_of_values['center_box_min'] > dict_of_values['center_box_max']:
            print('Warning!! We mannualy override your box boundary')
            dict_of_values['center_box_min'] = 0.5*dict_of_values['center_box_max']
        dict_of_values.update({'center_box': (dict_of_values['center_box_min'], dict_of_values['center_box_max'])})
        del dict_of_values['center_box_min']
        del dict_of_values['center_box_max']
    return dict_of_values



