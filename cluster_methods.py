from sklearn import cluster
from sklearn.model_selection import GridSearchCV
import os
import numpy as np
from Src.generate_cluster import make_a_cluster

def apply_clustering_method(samples, method_type, random_state=None, n_jobs=np.max([1, np.int(os.cpu_count()/4)]), **kwargs):

    __name__ = r"apply_clustering_method"
    __doc__ = r"a helper function to run different clustering method"
    """
    :param sammples:
    :param method_type:
    :param random_state:
    :param hyper_param_opt:
    :param kwargs:
    :return:
    """
    cluster_method_avaliable = dict(KMeans=cluster.KMeans,
                                    MiniBatchKMeans=cluster.MiniBatchKMeans,
                                    MeanShift=cluster.MeanShift,
                                    Birch=cluster.Birch,
                                    DBSCAN=cluster.DBSCAN,
                                    AffinityPropagation=cluster.AffinityPropagation,
                                    Agglomerative=cluster.AgglomerativeClustering,
                                    SpectralClustering=cluster.SpectralClustering)
    try:
        method = cluster_method_avaliable[method_type]
    except KeyError:
        print("We are sorry! We could not generate cluster type you are requested. The available type are: "
              "{0}".format(", ".join(cluster_method_avaliable.keys())))
        exit(-1)

    if method_type == 'MeanShift':
        mean_shift_band_width= cluster.estimate_bandwidth(samples, quantile=0.3)

    else:
        mean_shift_band_width = 0.4

    default_values = dict(KMeans={'init':'k-means++', 'max_iter':500, 'tol':1e-4, 'precompute_distances':'auto', 'random_state':random_state, 'algorithm':'auto','n_jobs':n_jobs},
                          MiniBatchKMeans={'n_clusters':8, 'init':'k-means++', 'max_iter':500, 'batch_size':100, 'compute_labels':True, 'random_state':random_state},
                          MeanShift= {'bandwidth':mean_shift_band_width,  'cluster_all':True},
                          Birch={},
                          DBSCAN={'eps':0.3, 'algorithm':'auto', 'p':4 },
                          AffinityPropagation={'damping':0.7, 'max_iter':500},
                          Agglomerative={'linkage':'ward'},
                          SpectralClustering={'eigen_solver':'amg'}
                          )
    if len(kwargs) > 0:
        default_base = default_values[method_type].copy()
        default_base.update(kwargs)
    else:
        default_base =default_values[method_type].copy()
    cls = method(**default_base)
    labels = cls.fit(samples).labels_

    return labels

if  __name__ =='__main__':
 pass
